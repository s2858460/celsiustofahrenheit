<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
	<head>
		<title>Celsius to Fahrenheit</title>
	</head>
	<body>
		<h1>Input celsius</h1>
		<input type="text" id="celsius_input" />
		<input type="button" id="convert_button" onclick="convert()" value="Convert"/>
		<script>
            function convert() {
                const input = document.getElementById("celsius_input");
                const value = parseFloat(input.value);
                const fahrenheit = value * 1.8 + 32.0;

                document.body.innerHTML = "<h1>" + fahrenheit + " Fahrenheit</h1>";
			}
		</script>
		<style>
			 #convert_button {
				border: 0;
				line-height: 2.5;
				padding: 0 20px;
				font-size: 1rem;
				text-align: center;
				color: #fff;
				text-shadow: 1px 1px 1px #000;
				border-radius: 10px;
				background-color: rgba(220, 0, 0, 1);
				background-image: linear-gradient(to top left,
												  rgba(0, 0, 0, .2),
												  rgba(0, 0, 0, .2) 30%,
												  rgba(0, 0, 0, 0));
				box-shadow: inset 2px 2px 3px rgba(255, 255, 255, .6),
							inset -2px -2px 3px rgba(0, 0, 0, .6);
			}

			#convert_button:hover {
				background-color: rgba(255, 0, 0, 1);
			}

			#convert_button:active {
				box-shadow: inset -2px -2px 3px rgba(255, 255, 255, .6),
							inset 2px 2px 3px rgba(0, 0, 0, .6);
			}
		</style>
	</body>
</html>